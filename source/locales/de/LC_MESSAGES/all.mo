��            )         �     �     �     �     �     �     �     �     �               /     =     B     R  
   e     p     s     {     �     �  	   �  
   �     �     �  	   �     �     �     �            9       Y     ^     |  	   �     �     �     �     �     �  %   �          +     0     I     i     z     }     �     �  !   �     �  
   �  +   �          ,     ;     S     Y     `  .   l        
                                                                                 	                                               About Add to queue Albums Cancel Community playlists Could not rename Delete Delete playlist? Donate Download to Music folder Enter name... More New playlist... No playlists found No results Ok Patrons Play Play all Playlist already exists Playlists Radio mode Remove from downloads Remove from queue Rename... Save to library Songs Videos Volume translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
 Info Zur Warteschlange hinzufügen Alben Abbrechen Community-Wiedergabelisten Umbenennen nicht möglich Löschen Wiedergabeliste löschen? Spenden In den »Musik«-Ordner herunterladen Einen Namen eingeben … Mehr Neue Wiedergabeliste … Keine Wiedergabelisten gefunden Keine Ergebnisse OK Patrons Wiedergeben Alle wiedergeben Wiedergabeliste existiert bereits Wiedergabelisten Radiomodus Aus den heruntergeladenen Dateien entfernen Aus der Warteschlange entfernen Umbenennen … In Bibliothek speichern Titel Videos Lautstärke Jürgen Benvenuti <gastornis@posteo.org>, 2023 